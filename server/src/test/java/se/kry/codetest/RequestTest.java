package se.kry.codetest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import io.vertx.core.json.JsonObject;

class RequestTest {

    @Test
    public void mapPostToExpectedValues() {
        final JsonObject bodyAsJson = new JsonObject();
        final String someName = "someName";
        bodyAsJson.put("name", someName);
        final String someUrl = "someUrl";
        bodyAsJson.put("url", someUrl);
        final String someUserId = "someUserId";

        final JsonObject result = Request.mapPost(bodyAsJson, someUserId);

        assertEquals(result.getString("name"), someName);
        assertEquals(result.getString("url"), someUrl);
        assertEquals(result.getString("userId"), someUserId);
    }

    @Test
    public void mapPutToExpectedValues() {
        final JsonObject bodyAsJson = new JsonObject();
        final String someName = "someName";
        bodyAsJson.put("name", someName);
        final String someUrl = "someUrl";
        bodyAsJson.put("url", someUrl);
        final String someUserId = "someUserId";
        final String someId = "someId";

        final JsonObject result = Request.mapPut(bodyAsJson, someId, someUserId);

        assertEquals(result.getString("name"), someName);
        assertEquals(result.getString("url"), someUrl);
        assertEquals(result.getString("userId"), someUserId);
        assertEquals(result.getString("id"), someId);
    }

    @Test
    public void mapDeleteToExpectedValues() {
        final JsonObject bodyAsJson = new JsonObject();
        final String someUserId = "someUserId";
        bodyAsJson.put("userId", someUserId);
        final String someId = "someId";
        bodyAsJson.put("id", someId);

        final JsonObject result = Request.mapDelete(someId, someUserId);

        assertEquals(result.getString("userId"), someUserId);
        assertEquals(result.getString("id"), someId);
    }

    @Test
    public void mapGetToExpectedValues() {
        final JsonObject bodyAsJson = new JsonObject();
        final String someUserId = "someUserId";
        bodyAsJson.put("userId", someUserId);

        final JsonObject result = Request.mapGet(someUserId);

        assertEquals(result.getString("userId"), someUserId);
    }
}