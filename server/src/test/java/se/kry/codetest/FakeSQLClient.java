package se.kry.codetest;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;

public class FakeSQLClient implements SQLClient {

    public Handler<AsyncResult<SQLConnection>> handlerGetConnection;
    public Handler<AsyncResult<Void>> handlerClose;
    public boolean closed;
    public String sql;
    public JsonArray params;
    public Handler<AsyncResult<UpdateResult>> handlerUpdateResult;
    public Handler<AsyncResult<ResultSet>> handlerResultSet;

    @Override public SQLClient getConnection(final Handler<AsyncResult<SQLConnection>> handler) {
        this.handlerGetConnection = handler;
        return null;
    }

    @Override public void close(final Handler<AsyncResult<Void>> handler) {
        this.handlerClose = handler;
    }

    @Override public void close() {
        this.closed = true;
    }

    @Override public SQLClient updateWithParams(String sql, JsonArray params, Handler<AsyncResult<UpdateResult>> handler) {
        this.sql = sql;
        this.params = params;
        handler.handle(Future.succeededFuture());
        this.handlerUpdateResult = handler;
        return this;
    }

    @Override public SQLClient queryWithParams(String sql, JsonArray params, Handler<AsyncResult<ResultSet>> handler) {
        this.sql = sql;
        this.params = params;
        return this;
    }
}
