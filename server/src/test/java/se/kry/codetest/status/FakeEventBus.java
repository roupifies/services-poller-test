package se.kry.codetest.status;

import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryContext;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.eventbus.MessageProducer;

public class FakeEventBus implements EventBus {

    public Object message;
    public String address;

    @Override public EventBus send(final String address, @Nullable final Object message) {
        this.message = message;
        this.address = address;
        return null;
    }

    @Override public <T> EventBus send(final String address, @Nullable final Object message, final Handler<AsyncResult<Message<T>>> replyHandler) {
        return null;
    }

    @Override public EventBus send(final String address, @Nullable final Object message, final DeliveryOptions options) {
        return null;
    }

    @Override public <T> EventBus send(final String address, @Nullable final Object message, final DeliveryOptions options, final Handler<AsyncResult<Message<T>>> replyHandler) {
        return null;
    }

    @Override public EventBus publish(final String address, @Nullable final Object message) {
        return null;
    }

    @Override public EventBus publish(final String address, @Nullable final Object message, final DeliveryOptions options) {
        return null;
    }

    @Override public <T> MessageConsumer<T> consumer(final String address) {
        return null;
    }

    @Override public <T> MessageConsumer<T> consumer(final String address, final Handler<Message<T>> handler) {
        return null;
    }

    @Override public <T> MessageConsumer<T> localConsumer(final String address) {
        return null;
    }

    @Override public <T> MessageConsumer<T> localConsumer(final String address, final Handler<Message<T>> handler) {
        return null;
    }

    @Override public <T> MessageProducer<T> sender(final String address) {
        return null;
    }

    @Override public <T> MessageProducer<T> sender(final String address, final DeliveryOptions options) {
        return null;
    }

    @Override public <T> MessageProducer<T> publisher(final String address) {
        return null;
    }

    @Override public <T> MessageProducer<T> publisher(final String address, final DeliveryOptions options) {
        return null;
    }

    @Override public EventBus registerCodec(final MessageCodec codec) {
        return null;
    }

    @Override public EventBus unregisterCodec(final String name) {
        return null;
    }

    @Override public <T> EventBus registerDefaultCodec(final Class<T> clazz, final MessageCodec<T, ?> codec) {
        return null;
    }

    @Override public EventBus unregisterDefaultCodec(final Class clazz) {
        return null;
    }

    @Override public void start(final Handler<AsyncResult<Void>> completionHandler) {

    }

    @Override public void close(final Handler<AsyncResult<Void>> completionHandler) {

    }

    @Override public <T> EventBus addOutboundInterceptor(final Handler<DeliveryContext<T>> interceptor) {
        return null;
    }

    @Override public <T> EventBus removeOutboundInterceptor(final Handler<DeliveryContext<T>> interceptor) {
        return null;
    }

    @Override public <T> EventBus addInboundInterceptor(final Handler<DeliveryContext<T>> interceptor) {
        return null;
    }

    @Override public <T> EventBus removeInboundInterceptor(final Handler<DeliveryContext<T>> interceptor) {
        return null;
    }
}
