package se.kry.codetest.status;


import static se.kry.codetest.EvenBus.GET_ALL_SERVICES_STATUS;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StatusServiceTest {

    private FakeEventBus eventBus = new FakeEventBus();

    private StatusService testSubject = new StatusService(eventBus);

    @Test
    public void sendExpectedMessageToExpectedAddressInEventBus() {
        testSubject.check();
        Assertions.assertEquals(eventBus.address, GET_ALL_SERVICES_STATUS.name());
        Assertions.assertEquals(eventBus.message, GET_ALL_SERVICES_STATUS.name());
    }

}
