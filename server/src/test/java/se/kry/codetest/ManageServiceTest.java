package se.kry.codetest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import io.vertx.junit5.VertxExtension;
import se.kry.codetest.status.FakeEventBus;

@ExtendWith(VertxExtension.class)
class ManageServiceTest {

    ManageService manageService;
    final FakeEventBus fakeEventBus = new FakeEventBus();
    final FakeDbConnector fakeDbConnector = new FakeDbConnector();

    @BeforeEach
    public void setup() {
        manageService = new ManageService(fakeDbConnector, fakeEventBus);
    }

    @Test
    void invokeConnectorWithExpectedUserIdToGetAllServices() {
        manageService.getAll("someUserId");
        Assertions.assertEquals(fakeDbConnector.query, "SELECT * FROM service where enabled = 1 and userId = 'someUserId';");
    }

    @Test
    void invokeConnectorWithExpectedQueryToGetAllServices() {
        manageService.getAll();
        Assertions.assertEquals(fakeDbConnector.query, "SELECT * FROM service where enabled = 1;");
    }

    @Test
    void invokeConnectorWithExpectedIdAndUserIdToDeleteService() {
        manageService.delete("someId", "someUserId");
        Assertions.assertEquals(fakeDbConnector.updateQuery, "UPDATE service SET enabled = 0 WHERE service_id='someId' and userId = 'someUserId';");
    }

    @Test
    void invokeConnectorWithExpectedIdAndUserIdToAddService() {
        manageService.add("someName","someUrl", "someUserId");
        Assertions.assertTrue(fakeDbConnector.updateQuery.contains("INSERT INTO service (service_id, name, url, status, date, enabled, userId) VALUES ("));
        Assertions.assertTrue(fakeDbConnector.updateQuery.contains("someName"));
        Assertions.assertTrue(fakeDbConnector.updateQuery.contains("someUrl"));
        Assertions.assertTrue(fakeDbConnector.updateQuery.contains("someUserId"));
    }

    @Test
    void invokeConnectorWithExpectedQueryToUpdateService() {
        manageService.update("someName", "someUrl", "someId", "someUserId");
        Assertions.assertEquals(fakeDbConnector.updateQuery, "UPDATE service SET name = 'someName', url = 'someUrl' WHERE service_id='someId' and userId = 'someUserId';");
    }

    @Test
    void invokeConnectorWithExpectedQueryToUpdateServiceStatus() {
        manageService.updateStatus("someUrl", "someStatus");
        Assertions.assertEquals(fakeDbConnector.updateQuery, "UPDATE service SET status = 'someStatus' WHERE url='someUrl';");
    }
}
