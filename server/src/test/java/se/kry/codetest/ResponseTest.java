package se.kry.codetest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import io.vertx.core.AsyncResult;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;

class ResponseTest {

    @Test
    public void mapAsyncResultToJsonObject() {
        final String someId = "someId";
        final String someName = "someName";
        final String someUrl = "someUrl";
        final String someStatus = "someStatus";
        final String someDate = "someDate";
        final ResultSet resultSet = mockResultSet(0, someId, someName, someUrl, someStatus, someDate);
        AsyncResult<io.vertx.ext.sql.ResultSet> asyncResultSet = new AsyncResult<ResultSet>() {
            @Override public ResultSet result() {
                return resultSet;
            }

            @Override public Throwable cause() {
                return null;
            }

            @Override public boolean succeeded() {
                return false;
            }

            @Override public boolean failed() {
                return false;
            }
        };
        final List<JsonObject> result = Response.map(asyncResultSet);

        final JsonObject resultEntries = result.get(0);
        assertEquals(resultEntries.getString("id"), someId.concat("0"));
        assertEquals(resultEntries.getString("name"), someName.concat("0"));
        assertEquals(resultEntries.getString("url"), someUrl.concat("0"));
        assertEquals(resultEntries.getString("status"), someStatus.concat("0"));
        assertEquals(resultEntries.getString("date"), someDate.concat("0"));

    }

    private ResultSet mockResultSet(final Integer index, final String someId, final String someName, final String someUrl, final String someStatus, final String someDate) {
        ResultSet resultSet = new ResultSet();
        final List<JsonArray> results = new ArrayList<>();
        final JsonArray element = new JsonArray();
        element.add(someId+index);
        element.add(someName+index);
        element.add(someUrl+index);
        element.add(someStatus+index);
        element.add(someDate+index);
        results.add(index, element);
        resultSet.setResults(results);
        return resultSet;
    }
}