package se.kry.codetest;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;

public class FakeDbConnector implements IDBConnector {

    public String updateQuery;
    public String query;
    public String queryWithParam;
    public JsonArray params;

    @Override public void update(final String query) {
        updateQuery = query;
    }

    @Override public Future<ResultSet> query(final String query) {
        this.query = query;
        return null;
    }

    @Override public Future<ResultSet> query(final String query, final JsonArray params) {
        queryWithParam = query;
        this.params = params;
        return null;
    }
}