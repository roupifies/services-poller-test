package se.kry.codetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static se.kry.codetest.EvenBus.PUSH_RESULT;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.vertx.core.json.JsonArray;
import se.kry.codetest.status.FakeEventBus;

class DBConnectorTest {

    DBConnector testSubject;
    final FakeSQLClient fakeSQLClient = new FakeSQLClient();
    final FakeEventBus fakeEventBus = new FakeEventBus();

    @BeforeEach
    public void setup() {
        testSubject = new DBConnector(fakeSQLClient, fakeEventBus);
    }

    @Test
    public void updateWithExpectedParams() {
        testSubject.update("someQuery");
        assertEquals(fakeSQLClient.sql, "someQuery");
        assertEquals(fakeSQLClient.params, new JsonArray());
        assertEquals(fakeEventBus.message, PUSH_RESULT.name());
        assertEquals(fakeEventBus.address, PUSH_RESULT.name());
    }

    @Test
    public void queryWithExpectedParams() {
        final JsonArray someJsonArray = new JsonArray();
        final String someQuery = "someQuery;";
        testSubject.query(someQuery, someJsonArray);
        assertEquals(fakeSQLClient.sql, someQuery);
        assertEquals(fakeSQLClient.params, someJsonArray);
    }

    @Test
    public void queryWithExpectedQuery() {
        final String someQuery = "someQuery;";
        testSubject.query(someQuery);
        assertEquals(fakeSQLClient.sql, someQuery);
        assertEquals(fakeSQLClient.params, new JsonArray());
    }
}