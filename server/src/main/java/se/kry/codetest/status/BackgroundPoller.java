package se.kry.codetest.status;

import static se.kry.codetest.EvenBus.CHECK_STATUS;
import static se.kry.codetest.EvenBus.UPDATE_STATUS;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.RequestOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

import java.util.concurrent.TimeUnit;

public class BackgroundPoller {

    private static final int FIVE_SECONDS_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(5);
    WebClient client;
    EventBus eventBus;
    private Vertx vertx;

    public BackgroundPoller(final Vertx vertx) {
        this.eventBus = vertx.eventBus();
        this.vertx = vertx;
        backgroundPollerEvent();
    }

    private void pollService(final String url) {
        final JsonObject entries = new JsonObject();
        entries.put("url", url);
        client = WebClient.create(vertx, buildClientOption(url));
        try {
            client
                .getAbs(url)
                .timeout(FIVE_SECONDS_TIMEOUT)
                .send(result -> {
                    entries.put("status", result.succeeded() ? "Ok" : "failed");
                    eventBus.send(UPDATE_STATUS.name(), entries);
                });
        } catch (Exception ignored) {
        }

    }

    private WebClientOptions buildClientOption(final String url) {
        WebClientOptions options = new WebClientOptions()
            .setUserAgent("Seifipour");
        options.setKeepAlive(false);
        options.setSsl(url.contains("https"));
        return options;
    }

    private void backgroundPollerEvent() {
        eventBus.consumer(CHECK_STATUS.name(), req -> pollService(req.body().toString()));
    }
}
