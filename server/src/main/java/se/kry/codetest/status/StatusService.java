package se.kry.codetest.status;

import io.vertx.core.eventbus.EventBus;
import static se.kry.codetest.EvenBus.GET_ALL_SERVICES_STATUS;

public class StatusService {

    private EventBus eb;

    public StatusService(final EventBus eb) {
        this.eb = eb;
    }

    public void check() {
        eb.send(GET_ALL_SERVICES_STATUS.name(), GET_ALL_SERVICES_STATUS.name());
    }

}
