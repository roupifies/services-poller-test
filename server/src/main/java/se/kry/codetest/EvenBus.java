package se.kry.codetest;

public enum EvenBus {
    GET_ALL_SERVICES_STATUS,
    CHECK_STATUS,
    PUSH_RESULT,
    UPDATE_STATUS,
    UPDATE,
    ADD,
    DELETE,
    SERVICES
}
