package se.kry.codetest;

import static se.kry.codetest.EvenBus.ADD;
import static se.kry.codetest.EvenBus.CHECK_STATUS;
import static se.kry.codetest.EvenBus.DELETE;
import static se.kry.codetest.EvenBus.GET_ALL_SERVICES_STATUS;
import static se.kry.codetest.EvenBus.PUSH_RESULT;
import static se.kry.codetest.EvenBus.SERVICES;
import static se.kry.codetest.EvenBus.UPDATE;
import static se.kry.codetest.EvenBus.UPDATE_STATUS;

import java.util.Date;
import java.util.UUID;

import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;

public class ManageService {

    private static final String GET_ALL_SERVICES_FOR_USER_QUERY = "SELECT * FROM service where enabled = 1 and userId = '%s';";
    private static final String GET_ALL_SERVICES_QUERY = "SELECT * FROM service where enabled = 1;";
    private static final String DELETE_SERVICE_QUERY = "UPDATE service SET enabled = 0 WHERE service_id='%s' and userId = '%s';";
    private static final String UPDATE_SERVICE_QUERY = "UPDATE service SET name = '%s', url = '%s' WHERE service_id='%s' and userId = '%s';";
    private static final String POST_SERVICE_QUERY = "INSERT INTO service (service_id, name, url, status, date, enabled, userId) VALUES ('%s', '%s' , '%s', 'UNKNOW', '%s', 1, '%s');";
    private static final String UPDATE_SERVICE_STATUS_QUERY = "UPDATE service SET status = '%s' WHERE url='%s';";
    private IDBConnector connector;
    private EventBus eb;

    ManageService(final IDBConnector connector, final EventBus eb) {
        this.connector = connector;
        this.eb = eb;
        registerConsumers();
    }

    public Future<ResultSet> getAll(final String userId) {
        return connector.query(String.format(GET_ALL_SERVICES_FOR_USER_QUERY, userId));
    }

    public Future<ResultSet> getAll() {
        return connector.query(GET_ALL_SERVICES_QUERY);
    }

    public void delete(final String id, final String userId) {
        connector.update(String.format(DELETE_SERVICE_QUERY, id, userId));
    }

    public void add(final String name, final String url, final String userId) {
        final String id = UUID.randomUUID().toString();
        connector.update(String.format(POST_SERVICE_QUERY, id, name, url, new Date(), userId));
    }

    public void update(final String name, final String url, final String id, final String userId) {
        connector.update(String.format(UPDATE_SERVICE_QUERY, name, url, id, userId));
    }

    public void updateStatus(final String url, final String status) {
        connector.update(String.format(UPDATE_SERVICE_STATUS_QUERY, status, url));
    }

    private void registerConsumers() {
        getAllServicesStatusEvent();
        updateStatusEvent();
        pushResultEvent();
        updateEvent();
        deleteEvent();
        addEvent();
    }

    private void getAllServicesStatusEvent() {
        eb.consumer(GET_ALL_SERVICES_STATUS.name(), (req) -> getAll().setHandler(result -> result.result().getResults().forEach(service -> {
            eb.send(CHECK_STATUS.name(), service.getValue(2));
        })));
    }

    private void updateEvent() {
        eb.consumer(UPDATE.name(), (req) -> {
            JsonObject requests = (JsonObject) req.body();
            update(requests.getString("name"), requests.getString("url"), requests.getString("id"), requests.getString("userId"));
        });
    }

    private void addEvent() {
        eb.consumer(ADD.name(), (req) -> {
            JsonObject requests = (JsonObject) req.body();
            add(requests.getString("name"), requests.getString("url"), requests.getString("userId"));
        });
    }

    private void deleteEvent() {
        eb.consumer(DELETE.name(), (req) -> {
            JsonObject requests = (JsonObject) req.body();
            delete(requests.getString("id"), requests.getString("userId"));
        });
    }

    private void updateStatusEvent() {
        eb.consumer(UPDATE_STATUS.name(), (req) -> {
            JsonObject requests = (JsonObject) req.body();
            updateStatus(requests.getString("url"), requests.getString("status"));
        });
    }

    private void pushResultEvent() {
        eb.consumer(PUSH_RESULT.name(), (req) -> getAll(Request.userId)
            .setHandler(result ->
                eb.send(SERVICES.name(), new JsonObject().put("result", Response.map(result)))));
    }
}
