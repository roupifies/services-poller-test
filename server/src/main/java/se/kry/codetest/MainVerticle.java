package se.kry.codetest;

import static se.kry.codetest.EvenBus.ADD;
import static se.kry.codetest.EvenBus.DELETE;
import static se.kry.codetest.EvenBus.SERVICES;
import static se.kry.codetest.EvenBus.UPDATE;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import se.kry.codetest.status.BackgroundPoller;
import se.kry.codetest.status.StatusService;

public class MainVerticle extends AbstractVerticle {

    private static final String EVENTBUS_PATH = "/eventbus/*";
    private static final int ONE_MINUTE_DELAY = 1000 * 60;

    @Override
    public void start(Future<Void> startFuture) {
        final EventBus eb = vertx.eventBus();

        new BackgroundPoller(vertx);
        final ManageService manageService = new ManageService(new DBConnector(vertx), eb);
        Router router = Router.router(vertx);
        router.route(EVENTBUS_PATH).handler(getSockJSHandler());
        router.route().handler(StaticHandler.create());
        router.route().handler(CookieHandler.create());
        vertx.setPeriodic(ONE_MINUTE_DELAY, timerId -> new StatusService(eb).check());
        router.route().handler(Request.createCors());
        setRoutes(router, eb, manageService);
        vertx
            .createHttpServer()
            .requestHandler(router)
            .requestHandler(router::accept)
            .listen(8080, result -> {
                if (result.succeeded()) {
                    System.out.println("KRY code test service started");
                    startFuture.complete();
                } else {
                    startFuture.fail(result.cause());
                }
            });
    }

    private SockJSHandler getSockJSHandler() {
        BridgeOptions opts = new BridgeOptions()
            .addOutboundPermitted(new PermittedOptions().setAddress(SERVICES.name()));
        return SockJSHandler.create(vertx).bridge(opts);
    }

    private void setRoutes(Router router, final EventBus eventBus, final ManageService manageService) {
        router.route("/*").handler(StaticHandler.create());
        router.post().handler(BodyHandler.create());
        router.put().handler(BodyHandler.create());
        router.get("/service").handler(req -> {
            final HttpServerResponse response = req.response();
            response.setChunked(true);
            final Future<ResultSet> serviceAllFuture = manageService.getAll();
            serviceAllFuture.setHandler(ar -> response
                .putHeader("content-type", "application/json")
                .end(new JsonArray(Response.map(ar)).encode()));
        });
        router.post("/service").handler(req -> {
            final String userId = Request.getUserId(req);
            eventBus.send(ADD.name(), Request.mapPost(req.getBodyAsJson(), userId));
            req.response().putHeader("content-type", "application/json").end(new JsonArray().encode());
        });
        router.delete("/service/:id").handler(req -> {
            final String userId = Request.getUserId(req);
            eventBus.send(DELETE.name(), Request.mapDelete(req.pathParam("id"), userId));
            req.response().putHeader("content-type", "application/json").end(new JsonArray().encode());
        });
        router.put("/service/:id").handler(req -> {
            final String userId = Request.getUserId(req);
            eventBus.send(UPDATE.name(), Request.mapPut(req.getBodyAsJson(), req.pathParam("id"), userId));
            req.response().putHeader("content-type", "application/json").end(new JsonArray().encode());
        });
    }
}
