package se.kry.codetest;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.CorsHandler;

public class Request {

    private static final String KRY_COOKIE_NAME = "KRY";
    public static String userId;

    public static CorsHandler createCors() {
        Set<String> allowedHeaders = new HashSet<>();
        allowedHeaders.add("x-requested-with");
        allowedHeaders.add("Access-Control-Allow-Origin");
        allowedHeaders.add("origin");
        allowedHeaders.add("Content-Type");
        allowedHeaders.add("accept");
        allowedHeaders.add("X-PINGARUNER");
        allowedHeaders.add("include");

        Set<HttpMethod> allowedMethods = new HashSet<>();
        allowedMethods.add(HttpMethod.GET);
        allowedMethods.add(HttpMethod.POST);
        allowedMethods.add(HttpMethod.DELETE);
        allowedMethods.add(HttpMethod.PUT);
        return CorsHandler.create("http://localhost:3000")
            .allowedHeaders(allowedHeaders)
            .allowedMethods(allowedMethods).allowCredentials(true);
    }

    public static JsonObject mapPost(final JsonObject bodyAsJson, final String userId) {
        final JsonObject entries = new JsonObject();
        entries.put("name", bodyAsJson.getString("name"));
        entries.put("url", bodyAsJson.getString("url"));
        entries.put("userId", userId);
        return entries;
    }

    public static JsonObject mapPut(final JsonObject bodyAsJson, final String id, final String userId) {
        final JsonObject entries = new JsonObject();
        entries.put("name", bodyAsJson.getString("name"));
        entries.put("url", bodyAsJson.getString("url"));
        entries.put("userId", userId);
        entries.put("id", id);
        return entries;
    }

    public static JsonObject mapDelete(final String id, final String userId) {
        final JsonObject entries = new JsonObject();
        entries.put("id", id);
        entries.put("userId", userId);
        return entries;
    }

    public static JsonObject mapGet(final String userId) {
        final JsonObject entries = new JsonObject();
        entries.put("userId", userId);
        return entries;
    }

    public static String getUserId(final RoutingContext req) {
        userId = Optional.ofNullable(req.getCookie(KRY_COOKIE_NAME)).map(Cookie::getValue)
            .orElseGet(() -> createCookie(req));
        return userId;
    }

    private static String createCookie(final RoutingContext req) {
        final String value = UUID.randomUUID().toString();
        final Cookie kry = Cookie.cookie(KRY_COOKIE_NAME, value);
        kry.setPath("/");
        kry.setDomain("http://localhost:3000/");
        final long fiveYearsAge = 158132000L;
        kry.setMaxAge(fiveYearsAge);
        req.addCookie(kry);
        return value;
    }

}
