package se.kry.codetest;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;

public interface IDBConnector {
    void update(String query);
    Future<ResultSet> query(String query);
    Future<ResultSet> query(String query, JsonArray params);
}
