package se.kry.codetest;

import java.util.List;
import java.util.stream.Collectors;

import io.vertx.core.AsyncResult;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;

public class Response {
    public static List<JsonObject> map(final AsyncResult<ResultSet> results) {
        return results.result().getResults().stream().map(service -> new JsonObject()
            .put("id", service.getValue(0))
            .put("name", service.getValue(1))
            .put("url", service.getValue(2))
            .put("status", service.getValue(3))
            .put("date", service.getValue(4)))
            .collect(Collectors.toList());
    }
}
