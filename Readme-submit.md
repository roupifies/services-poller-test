## How to Run
Go to the root of the project then
run `docker-compose up`

*OR*

run `./gradlew clean run` to start backend and `npm run install` and `npm run client` to start front-end

## Addressing issues

- Whenever the server is restarted, any added services disappear
    - store the services in the table name service

- There's no way to delete individual services
    - add delete functionality
- We want to be able to name services and remember when they were added
    - allow the user to name service and store the added date
- The HTTP poller is not implemented
    - use Vertx to check the status of the service every minute 

### Frontend/Web track:
- We want full create/update/delete functionality for services
    - added the functionality to allow the user do CRUD
- The results from the poller are not automatically shown to the user (you have to reload the page to see results)
    - add event bus to push the result to the user
- We want to have informative and nice looking animations on add/remove services
   - add loading gif

### Backend track
- Simultaneous writes sometimes cause strange behaviour
    - make writes async and also use event bus to queue the events
- Protect the poller from misbehaving services (for example answering really slowly)
    - add five-second time out to poller job
- Service URL's are not validated in any way ("sdgf" is probably not a valid service)
    - add regex validation to make sure URL is valid i.e. 
        - Start with Http or Https
        - A domain name contains only digits, characters and also dashes (-)
        - It should not start or end with a dash or have two dashes back to back
        - The subdomain is optional 
        - The TLDs should be between two to five characters
- A user (with a different cookie/local storage) should not see the services added by another user
    - Store websites against user unique id in the database


## Structure
The backend uses the Vertx and event bus to handle requests

#### Get 

- raise an async call to read the services of the user from the database

#### Post/Put/Delete

- raise a request to EventBus with details of the request and EventBus update the database in an async call and finally raise another event to push the result to the EventBus

Front end use react with redux to handle requests

#### Get

initially raise a get action to fetch data from backend on `componentDidMount` but keep listening to EventBus socks address to get notified about any changes on `componentDidUpdate`

#### /Post/Put/Delete

raise the action to the backend to update data and wait for EventBus to get it notified.

## More time
If I had more time, I would work on some nice to have functions to make it more scalable.

I would make sure EventBus, only push the updated result to front-end, and front-end, re-render partially the updated rows.

I would spend some time to make the poller job more efficient. 

We can use data to find out which website requires to check their status more often. 

for instance, we can use some factors such as the websites success rates, their added time, subdomains of domains, with high success rate, etc to priorities the status check.

I would use a cache layer in the backend to allow the poller job using it rather than hit the database every five minute.
