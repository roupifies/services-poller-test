import {LOAD_RESULT} from './Type';

const getServicesAction = () => (dispatch) => {
    fetch('http://localhost:8080/service/',{
        credentials: 'include'
    })
        .then(response => response.json())
        .then((data) => {
            dispatch({
                type: LOAD_RESULT,
                data,
            });
        })
        .catch([]);
};
export default getServicesAction;
