import { SERVICE_EDIT } from "./Type";

const EditServiceAction = (name, url, id) => (dispatch) => {
    fetch('http://localhost:8080/service/'+ id, {
        credentials: 'include',
        method: 'put',
        body: JSON.stringify({
            name: name,
            url: url
        })
    }).then(response => response.json())
        .then((data) => {
            dispatch({
                type: SERVICE_EDIT,
                data,
            });
        })
        .catch([]);
};

export default EditServiceAction;
