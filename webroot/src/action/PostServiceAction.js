import {SERVICE_CREATE} from "./Type";

const PostServiceAction = (postData) => (dispatch) => {
    fetch('http://localhost:8080/service', {
        credentials: 'include',
        method: 'post',
        body: JSON.stringify({
            name: postData.elements.serviceName.value,
            url: postData.elements.serviceUrl.value
        })
    }).then(response => response.json())
      .then((data) => {
           dispatch({
                type: SERVICE_CREATE,
                data,
            });
        })
        .catch([]);
};

export default PostServiceAction;
