import { DELETE } from "./Type";

const DeleteServiceAction = (id) => (dispatch) => {
    fetch('http://localhost:8080/service/'+ id, {
        credentials: 'include',
        method: 'DELETE'
    }).then(response => response.json())
        .then((data) => {
            dispatch({
                type: DELETE,
                data,
            });
        })
        .catch([]);
};

export default DeleteServiceAction;
