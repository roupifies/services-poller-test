import {LOAD_RESULT} from './Type';
import EventBus from 'vertx3-eventbus-client'

const GetUpdatedServiceAction = () => (dispatch) => {
    const eventBus = new EventBus("http://localhost:8080/eventbus");
    eventBus.enableReconnect(true);
    eventBus.onopen = () => {
        eventBus.registerHandler('SERVICES', function(error, message) {
            const data = message['body']['result'];
            dispatch({
                type: LOAD_RESULT,
                data,
            });
        });
    };
    eventBus.onclose = (param) => {
        console.log('closed', param)
    };
};
export default GetUpdatedServiceAction;
