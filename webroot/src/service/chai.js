import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());

// Make sure chai and jest ".not" play nice together
const originalNot = Object.getOwnPropertyDescriptor(chai.Assertion.prototype, 'not').get;

Object.defineProperty(chai.Assertion.prototype, 'not', {
  get() {
    Object.assign(this, this.assignedNot);

    return originalNot.apply(this);
  },
  set(newNot) {
    this.assignedNot = newNot;

    return newNot;
  },
});

// Combine both jest and chai matchers on expect
const jestExpect = global.expect;
const expect = (actual) => {
  const originalMatchers = jestExpect(actual);
  const chaiMatchers = chai.expect(actual);
  const combinedMatchers = Object.assign(chaiMatchers, originalMatchers);

  return combinedMatchers;
};

// return back extend's static methods
Object.keys(jestExpect).forEach((key) => {
  expect[key] = jestExpect[key];
});

export default expect;
