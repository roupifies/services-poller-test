import React from 'react';
import './ServiceCreate.css';

const pattern = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)+[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$";

const ServiceCreate = ({createService}) => {
    return (
        <form className="create-service" onSubmit={createService}>
            <div> name : <input type="text" name="serviceName" required/> </div>
            <div className="create-service service-url"> url :
                <input type="url" name="serviceUrl" id="url"
                       placeholder="https://example.com"
                       pattern={pattern} size="30"
                       required/> </div>
            <input className="create-button" type="submit" value="add"/>
            <div id="loading-animation" className="loading hide"/>
        </form>
    );
};

export default ServiceCreate;
