import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import store from '../../istore';
import Routers from './Routers';
import './App.scss';

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <div className="App">
        <main>
            <Routers/>
        </main>
      </div>
    </BrowserRouter>
  </Provider>
);

export default App;
