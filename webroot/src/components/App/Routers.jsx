import React from 'react';
import { Switch, Route, withRouter} from 'react-router-dom';
import ServiceList from '../ServiceList/ServiceList';

const Routers = () => (
  <Switch>
    <Route exact path="/" render={() => <ServiceList/>}/>
  </Switch>
);

export default withRouter(Routers);
