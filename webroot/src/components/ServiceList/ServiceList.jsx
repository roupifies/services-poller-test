import React, {Component} from 'react';
import getServicesAction from "../../action/GetServicesAction";
import PostServiceAction from "../../action/PostServiceAction";
import EditServiceAction from "../../action/EditServiceAction";
import DeleteServiceAction from "../../action/DeleteServiceAction";
import GetUpdatedServiceAction from "../../action/GetUpdatedServiceAction";
import {connect} from "react-redux";
import {withRouter} from 'react-router-dom';
import Service from "../Service/Service";
import ServiceCreate from "../ServiceCreate/ServiceCreate";
import EventBus from 'vertx3-eventbus-client'
import './ServiceList.css';

class ServiceList extends Component {
    constructor(props) {
        super(props);
        this.createService = this.createService.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.state = {services: []};

    }

    componentDidMount() {
        this.props.getServicesAction();
    }

    componentDidUpdate() {
        const eventBus = new EventBus("http://localhost:8080/eventbus");
        eventBus.enableReconnect(true);
        eventBus.onopen = () => {
            eventBus.registerHandler('SERVICES', () => {
                this.props.getServicesAction();
                this.hideLoading();
            });
        };
        eventBus.onclose = (param) => {
            console.log('closed', param);
        };
    }

    createService(event) {
        event.preventDefault();
        this.showLoading();
        this.props.PostServiceAction(event.target);
        event.target.elements.serviceName.value = '';
        event.target.elements.serviceUrl.value = '';
    }

    onDelete(id) {
        this.showLoading();
        this.props.DeleteServiceAction(id);
    }

    onEdit(name, url, id) {
       this.showLoading();
        this.props.EditServiceAction(name, url, id);
    }

    hideLoading() {
        let classList = document.getElementById("loading-animation").classList;
        if (!classList.contains("hide")) {
            classList.add("hide");
        }
    }

    showLoading() {
        let classList = document.getElementById("loading-animation").classList;
        if (classList.contains("hide")) {
            classList.remove("hide");
        }
    }

    render() {
        const {services} = this.props;
        return (
            <div className="services">
                <div className="services title"> List Of Services</div>
                <ServiceCreate createService={this.createService}/>
                {services.map((serviceDetails, index) => <Service key={index}
                                                                  serviceDetails={serviceDetails}
                                                                  onDelete={this.onDelete}
                                                                  onEdit={this.onEdit}/>)}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    services: state.servicesReducer.services
});

export default withRouter(connect(mapStateToProps, {
    getServicesAction,
    PostServiceAction,
    EditServiceAction,
    DeleteServiceAction,
    GetUpdatedServiceAction
})(ServiceList));

