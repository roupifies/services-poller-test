import React from 'react';
import { shallow } from 'enzyme';
import expect from "../../service/chai";
import configureStore from "redux-mock-store";
import * as mockServiceList from "../../data/mock-servicelist";

import ServiceList from './ServiceList';

describe('ServiceList', () => {
  test('should match snapshot', () => {
    const mockStore = configureStore([]);
    const store = mockStore(mockServiceList);
    const wrapper = shallow(<ServiceList store={store} />);

    expect(wrapper).toMatchSnapshot();
  });
});
