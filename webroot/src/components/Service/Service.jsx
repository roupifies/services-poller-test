import React, {useState} from 'react';
import Moment from 'moment';
import './Service.scss';

const Service = ({serviceDetails, index, onDelete, onEdit}) => {

    const [editMode, handleEditMode] = useState(false);
    let {name, url, status, id, date} = serviceDetails;

    return (
        <div key={index} className="service">
            { !editMode ?
                 <div>
                    <table className="content">
                        <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>{name}</td>
                            <td>{status}</td>
                            <td>{Moment(date).format('YYYY MMM DD, HH:MM')}</td>
                            <td><input className="button" type="button" value="edit" onClick={() => handleEditMode(!editMode)}/></td>
                            <td><input className="button" type="button" value="delete" onClick={() => onDelete(id)}/></td>
                        </tr>
                        </tbody>
                    </table>
                  </div>
                :
                <div>
                <table className="content">
                    <tbody>
                    <tr>
                        <th>Name</th>
                        <th>URL</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td><input className="input-content" type="text" placeholder={name} onChange={(event) => name = event.target.value}/></td>
                        <td><input className="input-content" type="text" placeholder={url} onChange={(event) => url = event.target.value}/></td>
                       <td>
                           <input className="button" type="button" value="update" onClick={() => { handleEditMode(!editMode); onEdit(name, url,id)}}/>
                       </td>
                     </tr>
                    </tbody>
                </table>
                </div>

            }
        </div>
    );
};

export default Service;
