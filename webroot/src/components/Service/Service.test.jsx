import React from 'react';
import { shallow } from 'enzyme';
import expect from '../../service/chai';

import Service from './Service';


describe('Service', () => {
  test('should match snapshot', () => {
    const serviceDetails = {
        name: 'test-name',
        url: 'test-url',
        status: 'test-status'
    }
    expect(shallow(<Service serviceDetails={serviceDetails}/>)).toMatchSnapshot();
  });
});
