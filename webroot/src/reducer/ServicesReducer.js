import {LOAD_RESULT, DELETE, SERVICE_CREATE, SERVICE_EDIT} from '../action/Type';

const initialRepositoryState = {
   // services: [{'name': 'some init name', 'url': 'some init url', 'date': 'some init date', 'status':'some init status'}]
    services:[]
};

export default (state = initialRepositoryState, action) => {
    switch (action.type) {
        case LOAD_RESULT:
            return {
                ...state,
                services: action.data,
            };
        case DELETE:
            return {
                ...state,
                services: action.data,
            };
        case SERVICE_CREATE:
            return {
                ...state,
                services: action.data,
            };
        case SERVICE_EDIT:
            return {
                ...state,
                services: action.data,
            };
        default:
            return state;
    }
};
