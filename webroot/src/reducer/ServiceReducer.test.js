import ServicesReducer from './ServicesReducer';
import {EDIT, SERVICE_CREATE, DELETE, LOAD_RESULT, SERVICE_EDIT} from '../action/Type';

describe('search result reducer', () => {
    let testSubject;

    it('return init status when action is not defined', () => {
        const someInitState = {};
        const notDefinedAction = { type: undefined };
        testSubject = ServicesReducer(someInitState, notDefinedAction);

        expect(testSubject).toEqual(someInitState);
    });

    it('add action data when action type is CREATE', () => {
        const someInitState = {
            services:[{ 'name': 'some data'}]
        };
        const definedAction = [{ type: SERVICE_CREATE, services: [{ 'name': 'some data'}] }];
        testSubject = ServicesReducer(someInitState, definedAction);

        expect(testSubject).toEqual({ services: [{ 'name': 'some data'}] });
    });

    it('add action data when action type is EDIT', () => {
        const someInitState = {
            services:[{ 'name': 'some data'}]
        };
        const definedAction = [{ type: SERVICE_EDIT, services: [{ 'name': 'some data'}] }];
        testSubject = ServicesReducer(someInitState, definedAction);

        expect(testSubject).toEqual({ services: [{ 'name': 'some data'}] });
    });

    it('add action data when action type is DELETE', () => {
        const someInitState = {
            services:[{ 'name': 'some data'}]
        };
        const definedAction = [{ type: DELETE, services: [{ 'name': 'some data'}] }];
        testSubject = ServicesReducer(someInitState, definedAction);

        expect(testSubject).toEqual({ services: [{ 'name': 'some data'}] });
    });
});
