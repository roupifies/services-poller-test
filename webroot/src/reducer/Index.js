import { combineReducers } from 'redux';
import servicesReducer from './ServicesReducer';

export default combineReducers({
  servicesReducer: servicesReducer,
});
